/**
 * Created by aldri on 17/11/2015.
 */
const gulp            = require('gulp');
const _               = require('underscore');
const mainBowerFiles  = require('gulp-main-bower-files');
const rename          = require('gulp-rename');
const concat          = require('gulp-concat');
const clean           = require('gulp-clean');
/*
    Javascript
 */
const coffee      = require('gulp-coffee');
const ngAnnotate  = require('gulp-ng-annotate');
const jsmin       = require('gulp-jsmin');
/*
    CSS
 */
const prefixer    = require('gulp-autoprefixer');
const minifyCSS   = require('gulp-minify-css');

const sass        = require('gulp-ruby-sass');

/*
    Essential
 */
const plumber     = require('gulp-plumber');

/*
  Clean Prod Files
*/
gulp.task('clean',function(){
  //Remove Bower Components
  gulp.src('bower_components')
      .pipe(clean());
  //Remove Processes Components
  gulp.src('vendors')
      .pipe(clean());
});

/*
  Set Bower
*/
gulp.task('load-bower-files',function(){
  /*
    Include the UI Kit components
  */
  var overrides = {
    uikit:{
      main:[
        './css/uikit.css',
        './css/components/tooltip.css',
        './css/components/sticky.css',
        './js/uikit.js',
        './js/components/tooltip.js',
        './js/components/sticky.js'
        ]
      },
    'clamp.js': {
        main:[
          './clamp.js'
        ]
      }
  }

  gulp.src('./bower.json')
      .pipe(mainBowerFiles({overrides:overrides}))
      .pipe(gulp.dest('vendors'));

  gulp.src('bower_components/**/*.{ttf,otf,woff,woff2}')
      .pipe(rename({dirname:''}))
      .pipe(gulp.dest('assets/fonts'));
  gulp.src('bower_components/uikit/*.{png,jpg,jpeg,svg}')
      .pipe(rename({dirname:''}))
      .pipe(gulp.dest('assets/images'));

  /*
    Write the Dependencies and concat them
  */
  var vendorDest    = 'vendors/';
  var JSdependencies = {
      foundations: [
        vendorDest + 'jquery/dist/jquery.js',
        vendorDest + 'clamp.js/clamp.js',
        vendorDest + 'js-cookie/src/js.cookie.js'
      ],
      uiKit : [
        vendorDest + 'uikit/js/uikit.js',
        vendorDest + 'uikit/js/components/tooltip.js',
        vendorDest + 'uikit/js/components/sticky.js'
      ],
      angular : [
        vendorDest + 'angular/angular.js',
        vendorDest + 'angular-sanitize/angular-sanitize.js',
        vendorDest + 'angular-animate/angular-animate.js',
        vendorDest + 'angular-cookies/angular-cookies.js',
        vendorDest + 'nginfinitescroll/build/ng-infinite-scroll.js',
      ]
    }

  var JSdependenciesFlat = _.flatten([
    JSdependencies.foundations,
    JSdependencies.uiKit,
    JSdependencies.angular
  ]);

  gulp.src(JSdependenciesFlat)
      .pipe(plumber())
      .pipe(ngAnnotate({single_quotes:true}))
      .pipe(concat('js-dependencies.js'))
      .pipe(jsmin())
      .pipe(gulp.dest('assets/js'));

  var cssDependencies = [
    vendorDest + 'uikit/css/uikit.css',
    vendorDest + 'uikit/css/components/tooltip.css',
    vendorDest + 'uikit/css/components/stick.css'
  ];
  gulp.src(cssDependencies)
      .pipe(plumber())
      .pipe(concat('css-essentials.css'))
      .pipe(minifyCSS())
      .pipe(gulp.dest('assets/css'));
});

/*
    Run-once
 */

gulp.task('coffee-main',function(){
    gulp.src('app/src/**/*.coffee')
        .pipe(plumber())
        .pipe(coffee({bare: true}))
        .pipe(ngAnnotate({single_quotes:true}))
        .pipe(jsmin())
        .pipe(gulp.dest('app'));
});

/*
  Sass Components
*/
var processSass = function(config){
  return sass(config.src)
             .pipe(plumber())
             .on('error',sass.logError)
             .pipe(prefixer());
}

gulp.task("sass-template",function(){
  var config = {
    src :"assets/sass/template/**/*.sass",
    dest:"assets/css"
  };
  var task = processSass(config);
  task.pipe(minifyCSS())
      .pipe(gulp.dest(config.dest));
});

/*
    Run-watch
 */
gulp.task('watch',function(){
    gulp.watch('app/src/**/*.coffee',['coffee-main']);
    gulp.watch('assets/sass/**/*.sass',['sass-template']);
});

gulp.task('default',['coffee-main','sass-template','watch']);

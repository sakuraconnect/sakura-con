<?php
    get_header();

    $template = get_post_meta($post_id,'page_template',true);
    $template = (empty($template))?'default':$template;
    get_template_part('wp-template/page',$template);

    get_footer();

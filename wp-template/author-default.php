<?php $authorID = get_query_var('author'); ?>
<script>
    var authorID   = <?php print $authorID ?>;
    var catTitle   = "<?php print get_the_author_meta('display_name'); ?>";
</script>
<div data-uk-sticky style="z-index:10000">
    <?php get_template_part('wp-template/nav','mobile'); ?>
    <?php get_template_part('wp-template/nav','pc'); ?>
</div>
<main ng-controller="authorDefaultCtrl"
      id="skn-content">
    <!-- Category Title -->
    <section id="skn-author-meta">
        <div class="uk-width-medium-5-10
                    uk-width-small-1-1
                    author-meta-container">
            <div class="uk-small-hidden
                        uk-width-medium-1-4
                        uk-text-center">
                <?php print get_avatar($authorID,128,get_the_author_meta('display_name')); ?>
            </div>
            <div class="uk-width-small-1-1
                        uk-width-medium-3-4">
                <h1><?php print get_the_author_meta('display_name'); ?></h1>
                <p>
                    <i class="uk-icon uk-icon-twitter"></i> 
                    <?php if(!empty(get_the_author_meta('twitter'))): ?>
                        @<a href="https://twitter.com/intent/follow?screen_name=<?php print get_the_author_meta('twitter'); ?>"
                            onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                            target="_blank"><?php print get_the_author_meta('twitter'); ?></a>
                    <?php else: ?>
                        @<a href="https://twitter.com/intent/follow?screen_name=sakunnect"
                            onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                            target="_blank">sakunnect</a>
                    <?php endif; ?>                     
                    &bull;
                    <i class="uk-icon uk-icon-envelope-o"></i> 
                    <?php if(!empty(get_the_author_meta('user_email'))): ?>
                        <?php print get_the_author_meta('user_email'); ?>
                    <?php else: ?>
                        hello@sakuraconnect.co
                    <?php endif; ?>                    
                </p>
                <?php if(!empty(get_the_author_meta('description'))): ?>
                    <p><?php print get_the_author_meta('description'); ?></p>
                <?php else: ?>
                    <p>The author does not have a written profile in the site.</p>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section id="skn-author-items"
             infinite-scroll="loadMoreItems()"
             infinite-scroll-disabled="disableInfiniteScroll">
        <div class="uk-width-medium-9-10
                    uk-width-small-1-1
                    author-container">
            <item-basic ng-repeat="item in items" data="item"></item-basic>
        </div>
    </section>
</main>
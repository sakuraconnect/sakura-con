<div class="share">
    <ul>
        <li class="facebook" 
            data-uk-tooltip
            title="Share this on Facebook">
            <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>"
               target="_blank">
                <i class="uk-icon-facebook"></i> <span class="uk-hidden-small">Facebook</span>
            </a>
        </li>
        <li class="twitter"
            data-uk-tooltip
            title="Share this on Twitter">
            <a href="https://twitter.com/intent/tweet?text=<?php the_title(); ?>&amp;url=<?php the_permalink();?>&amp;via=sakunnect"
               onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
               target="_blank">
                <i class="uk-icon-twitter"></i> <span class="uk-hidden-small">Twitter</span>
            </a>
        </li>
        <li class="pinterest"
            data-uk-tooltip
            title="Share this on Pinterest">
            <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; ?>"
               data-pin-do="buttonBookmark"
               data-pin-custom="true"
               target="_blank">
                <i class="uk-icon-pinterest"></i> <span class="uk-hidden-small">Pinterest</span>
            </a>
        </li>
        <li class="google-plus"
            data-uk-tooltip
            title="Share this on Google+">
            <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>"
               onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                <i class="uk-icon-google-plus"></i> <span class="uk-hidden-small">Google+</span>
            </a>
        </li>
    </ul>
</div>
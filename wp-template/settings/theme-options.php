<div class="wrap">
    <h2>Sakura Connect Theme Options</h2>
    <form method="post" action="options.php">
        <?php
            settings_fields('skn-theme-options-group');
            do_settings_sections('sknThemeSettings');
            submit_button('Submit');
        ?>
    </form>
</div>
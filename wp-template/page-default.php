<div data-uk-sticky style="z-index:10000">
    <?php get_template_part('wp-template/nav','mobile'); ?>
    <?php get_template_part('wp-template/nav','pc'); ?>
</div>
<main ng-controller="singleDefaultCtrl"
      id="skn-content">
    <div class="uk-width-medium-9-10
                uk-width-small-1-1">
        <section id="content"
                 class="uk-width-small-1-1">
            <article class="uk-article uk-width-1-1">
                <h1 class="uk-article-title uk-margin-remove">
                    <?php the_title(); ?>
                </h1>
                <hr class="uk-article-divider"/>
                <section class="articleBody content-styles">
                    <?php the_content(); ?>
                </section>
                <hr class="uk-article-divider" />
            </article>
    </section>
    </div>
</main>
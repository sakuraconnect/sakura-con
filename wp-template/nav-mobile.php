<nav id="skn-mobile-nav"
     class="uk-hidden-large"
     ng-controller="mobileHeaderCtrl">
    <section class="nav">
        <button data-uk-offcanvas="{target:'#skn-off-maincanvas'}">
            <i class="uk-icon uk-icon-reorder"></i>
        </button>
    </section>
    <div class="brand">
        <a href="<?php bloginfo('url'); ?>"
           ng-show="!toggleSearch" >
            <?php if(is_home()) : ?>
                <h1><?php print bloginfo('name','display'); ?></h1>
            <?php else: ?>
                <p><?php print bloginfo('name','display'); ?></p>
            <?php endif;?>
            <p><?php print bloginfo('description','display'); ?></p>
        </a>
        <input
            ng-show="toggleSearch"
            type="search"
            placeholder="Search Reviews & Posts"
            ng-model="searchText"
            ng-keydown="searchPosts($event)"
            />
    </div>
    <section class="extra">
        <button ng-click="toggleSearchFunc()">
            <i class="uk-icon uk-icon-search"></i>
        </button>
    </section>

    <!-- Canvas Menu -->
    <section id="skn-off-maincanvas"
             class="uk-offcanvas">
        <?php
        $mainNavMenu = array(
            'theme_location'  => 'skn-mainnav-menu',
            'menu'            => 'name',
            'container'       => 'div',
            'container_class' => 'uk-offcanvas-bar',
            'container_id'    => '',
            'menu_class'      => 'uk-list',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        );
        wp_nav_menu($mainNavMenu);
        ?>
    </section>
</nav>
<search-page></search-page>
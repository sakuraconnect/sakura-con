<div data-uk-sticky style="z-index:10000">
    <?php get_template_part('wp-template/nav','mobile'); ?>
    <?php get_template_part('wp-template/nav','pc'); ?>
</div>
<main ng-controller="singleMediumCtrl"
      id="skn-content">
    <article class="uk-article">
        <!-- Feature Image Box -->
        <header class="uk-width-1-1"
                id="skn-feature-image"
                style="background-image: url('<?php print get_that_image($post_id); ?>')">
            <div class="overlay"></div>
            <div class="header-content">
                <h1><?php the_title(); ?></h1>
                <p>
                    Published <span class="datePublished"><?php the_date() ?> <?php the_time() ?></span> &bull;
                    Updated <span><?php the_modified_date() ?> <?php the_modified_time() ?></span>
                </p>
            </div>
        </header>
        <section class="uk-width-1-1"
                 id="skn-content-container">
            <div class="uk-width-small-1-1
                        uk-width-medium-9-10">
                <?php get_template_part('wp-template/share','buttons'); ?>
            </div>
            <div class="skn-content
                        uk-width-small-1-1
                        uk-width-medium-9-10
                        content-styles">
                <?php the_content(); ?>
            </div>
        </section>
    </article>
</main>
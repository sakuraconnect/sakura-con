<footer id="skn-main-footer">
    <div class="uk-width-9-10">
    <section class="brand">
        <img src="<?php print get_template_directory_uri(); ?>/assets/images/sakura-connect-white.png"/>
        <p><?php print get_option('skn_footer_about','I have a dream. A dream to have a description.'); ?></p>
        <p class="about">
            <a href="<?php print get_permalink(get_option('skn_footer_about_link',get_bloginfo('url'))); ?>"
               class="uk-button
                      uk-button-large">Read More</a>
        </p>
    </section>
    <section class="content">
        <?php
            for($x=1;$x <= 3;$x++):
                $mainFooterMenu = array(
                    'theme_location'  => 'skn-footernav-menu-'.$x,
                    'menu'            => 'name',
                    'container'       => false,
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => 'uk-list',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0,
                    'walker'          => ''
                );
        ?>
        <div class="list">
            <h3><?php print get_option('skn_footer_title_'.$x,'Footer '.$x); ?></h3>
            <?php wp_nav_menu($mainFooterMenu); ?>
        </div>
        <?php endfor; ?>
    </section>
    <section class="copyright">
        <p>Project Sakura &copy; 2016. &bull; <a href="/copyrights">Copyrights</a></p>
    </section>
    </div>
</footer>

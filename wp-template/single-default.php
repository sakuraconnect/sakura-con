<div data-uk-sticky style="z-index:10000">
<?php get_template_part('wp-template/nav','mobile'); ?>
<?php get_template_part('wp-template/nav','pc'); ?>
</div>
<main ng-controller="singleDefaultCtrl"
      id="skn-content">
    <div class="uk-width-medium-9-10
                uk-width-small-1-1">
        <section id="content"
                 class="uk-width-small-1-1
                        uk-width-medium-7-10">
        <article class="uk-article uk-width-1-1">
            <div class="uk-text-center
                        uk-width-1-1
                        pc-feature-image">
                <?php
                      if(check_if_video_post(get_the_ID())):
                          print "<div class='video-wrapper uk-margin-bottom'>";
                          $videoType = get_post_meta(get_the_ID(),'video_type',true);
                          if(empty($videoType) || $videoType == 'youtube'):
                ?>
                              <iframe src="//www.youtube.com/embed/<?php print get_post_meta(get_the_ID(),'video_id',true) ?>?autoplay=0"
                                      frameborder="0"
                                      allowfullscreen>
                              </iframe>
                              <?php else: ?>
                              <iframe src="//www.facebook.com/video/embed?video_id=<?php print get_post_meta(get_the_ID(),'video_id',true) ?>"
                                      frameborder="0"
                                      allowfullscreen></iframe>
                              <?php endif; ?>
                          </div>
                <?php else: ?>
                    <img class="uk-hidden-small
                                uk-margin-bottom"
                            src="<?php print get_that_image($post_id); ?>"/>
                <?php endif; ?>
            </div>
            <p class="uk-margin-remove">
                <?php $cats = get_the_category(); foreach( $cats as $cat ) { ?>
                <a class="category" href="<?php echo get_category_link($cat->term_id); ?>">
                <span>
                        <?php echo $cat->name; ?>
                </span>
                </a>
                <?php break;} ?>
            </p>
            <h1 class="uk-article-title">
                <?php the_title(); ?>
            </h1>
            <hr class="uk-article-divider" />
            <p class="uk-article-meta">
                <span class="uk-text-bold">
                    <a href="<?php print get_author_posts_url(get_the_author_meta('ID')) ?>"
                       target="_new">
                        <?php the_author_link(); ?></a>
                </span> <br />
                <i class="uk-icon uk-icon-twitter"></i>
                <span>
                    <?php if(!empty(get_the_author_meta('twitter'))): ?>
                        @<a href="https://twitter.com/intent/follow?screen_name=<?php print get_the_author_meta('twitter'); ?>"
                            onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                            target="_blank"><?php print get_the_author_meta('twitter'); ?></a>
                    <?php else: ?>
                        @<a href="https://twitter.com/intent/follow?screen_name=sakunnect"
                            onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                            target="_blank">sakunnect</a>
                    <?php endif; ?>
                </span>
                <br />
                Published <span class="datePublished"><?php the_date() ?> <?php the_time() ?></span> &bull;
                Updated <span><?php the_modified_date() ?> <?php the_modified_time() ?></span>
            </p>
            <?php if(!empty(get_that_image($post_id))): ?>
                <img class="uk-visible-small uk-margin-bottom"
                    src="<?php print get_that_image($post_id) ?>"/>
            <?php endif; ?>
            <?php if(has_excerpt()): ?>
            <p class="uk-article-lead">
                <?php print get_the_excerpt(); ?>
            </p>
            <?php endif; ?>
            <?php get_template_part('wp-template/share','buttons'); ?>
            <hr class="uk-article-divider"/>
            <section class="articleBody content-styles uk-clearfix">
                <?php the_content(); ?>
            </section>
            <hr class="uk-article-divider" />
            <div class="tags">
                <ul>
                    <?php
                        $tags = get_the_tags($post_id);
                        if(count($tags) > 1):
                            foreach($tags as $tag):
                    ?>
                    <li class="uk-margin-bottom">
                        <a href="<?php print get_tag_link($tag->term_id) ?>">
                            <?php print $tag->name ?>
                        </a>
                    </li>
                    <?php
                            endforeach;
                        endif;
                    ?>
                </ul>
            </div>
            <div class="uk-panel
                        uk-panel-box
                        uk-panel-box-primary
                        uk-width-1-1
                        uk-text-center
                        uk-h4
                        uk-text-strong">
                <i class="uk-icon uk-icon-envelope-o"></i>
                For any comments, suggestions and concerns on this post, send an email at comments@sakuraconnect.co
            </div>
        </article>
        </section>
        <aside class="uk-width-small-1-1-
                      uk-width-medium-3-10
                      skn-widget-board">
            <!-- Set Sticky before directive request -->
            <div data-uk-sticky="{top:75, media:'(min-width:721px)',boundary:true}">
                <tag-list post-id="<?php print get_the_ID(); ?>"></tag-list>
            </div>
        </aside>
    </div>
</main>

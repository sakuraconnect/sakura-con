<div class="follow">
    <ul>
        <li>
            Follow us on
        </li>
        <li class="facebook" 
            data-uk-tooltip
            title="Follow us on Facebook">
            <a href="http://www.facebook.com/sakunnect"
               target="_blank">
                <i class="uk-icon-facebook"></i> <span class="uk-hidden-small">Facebook</span>
            </a>
        </li>
        <li class="twitter"
            data-uk-tooltip
            title="Follow us on Twitter">
            <a href="https://twitter.com/intent/follow?user_id=4108162518"
               onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
               target="_blank">
                <i class="uk-icon-twitter"></i> <span class="uk-hidden-small">Twitter</span>
            </a>
        </li>
        <li class="pinterest"
            data-uk-tooltip
            title="Follow us on Pinterest">
            <a href="http://pinterest.com/sakunnect"
               data-pin-do="buttonBookmark"
               data-pin-custom="true"
               target="_blank">
                <i class="uk-icon-pinterest"></i> <span class="uk-hidden-small">Pinterest</span>
            </a>
        </li>
        <li class="google-plus"
            data-uk-tooltip
            title="Follow us on Google+"
            target="_blank">
            <a href="https://plus.google.com/110360388668090876612">
                <i class="uk-icon-google-plus"></i> <span class="uk-hidden-small">Google+</span>
            </a>
        </li>
        <li class="instagram"
            data-uk-tooltip
            title="Follow us on Instagram"
            target="_blank">
            <a href="https://instagram.com/sakunnect">
                <i class="uk-icon-google-plus"></i> <span class="uk-hidden-small">Instagram</span>
            </a>
        </li>
        <li class="youtube"
            data-uk-tooltip
            title="Subscribe on YouTube"
            target="_blank">
            <a href="https://www.youtube.com/channel/UC8x9IsXnlzRuT1kbYzlJVSA">
                <i class="uk-icon-google-plus"></i> <span class="uk-hidden-small">YouTube</span>
            </a>
        </li>        
    </ul>
</div>
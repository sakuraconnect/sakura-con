<?php $tagId = get_query_var('tag_id'); ?>
<script>
    var tagId       = <?php print $tagId;?>;
    var tagTitle    ="<?php single_tag_title('',true); ?>";
</script>
<div data-uk-sticky style="z-index:10000">
    <?php get_template_part('wp-template/nav','mobile'); ?>
    <?php get_template_part('wp-template/nav','pc'); ?>
</div>
<main id="skn-content"
      ng-controller="tagDefaultCtrl">
    <!-- Tag Title -->
    <section id="skn-tag-meta">
        <div class="uk-width-medium-9-10
                    uk-width-small-1-1
                    uk-margin-top">
            <h1>Topic: <?php single_tag_title('',true); ?></h1>
            <p>
                <i class="uk-icon-newspaper-o"></i>
                <?php
                    $category = get_queried_object();
                    echo $category->count;
                ?> Posts &bull; 
                <i class="uk-icon-rss"></i> 
                <a href="feed">Subscribe</a>
            </p>            
            <?php if(!empty(tag_description())): ?>
                <p><?php print category_description(); ?></p>
            <?php else: ?>
                <p>This is a topic page for <?php single_tag_title('',true); ?>, where everything related to it is located.</p>
            <?php endif; ?>
        </div>
    </section>
    <section id="skn-tag-items"
             infinite-scroll="loadMoreItems()"
             infinite-scroll-disabled="disableInfiniteScroll">
        <div class="uk-width-medium-9-10
                    uk-width-small-1-1
                    tag-container">
            <item-basic ng-repeat="item in items" data="item"></item-basic>
        </div>
    </section>
</main>
<!doctype html>
<html amp>
<head>
	<title><?php echo esc_html( $amp_post->get_title() ); ?> | <?php echo esc_html( get_bloginfo( 'name' ) ); ?></title>
	<meta charset="utf-8">
	<link rel="canonical" href="<?php echo esc_url( $amp_post->get_canonical_url() ); ?>" />
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no,minimal-ui">
	<link href='https://fonts.googleapis.com/css?family=Passion+One|Open+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
	<?php foreach ( $amp_post->get_scripts() as $element => $script ) : ?>
		<script custom-element="<?php echo esc_attr( $element ); ?>" src="<?php echo esc_url( $script ); ?>" async></script>
	<?php endforeach; ?>
	<script src="https://cdn.ampproject.org/v0.js" async <?php echo defined( 'AMP_DEV_MODE' ) && AMP_DEV_MODE ? 'development' : ''; ?>></script>
	<script type="application/ld+json"><?php echo json_encode( $amp_post->get_metadata() ); ?></script>
	<?php do_action( 'amp_head', $amp_post ); ?>
	<style>body {opacity: 0}</style><noscript><style>body {opacity: 1}</style></noscript>
	<style amp-custom>
	/* Generic WP styling */
	amp-img.alignright { float: right; margin: 0 0 1em 1em; }
	amp-img.alignleft { float: left; margin: 0 1em 1em 0; }
	amp-img.aligncenter { display: block; margin-left: auto; margin-right: auto; }
	.alignright { float: right; }
	.alignleft { float: left; }
	.aligncenter { display: block; margin-left: auto; margin-right: auto; }


	/* Generic WP.com reader style */
	.content, .title-bar div {
		max-width: 600px;
		margin: 0 auto;
	}

	body {
		font-family: 'Open Sans', Serif;
		font-size: 16px;
		line-height: 1.8;
		background: #fff;
		color: #3d596d;
		padding-bottom: 100px;
	}

	.content {
		padding: 16px;
		overflow-wrap: break-word;
		word-wrap: break-word;
		font-weight: 400;
		color: #3d596d;
	}

	.title {
		margin: 36px 0 0 0;
		font-size: 36px;
		line-height: 1.258;
		font-weight: 700;
		color: #2e4453;
	}

	.meta {
		margin-bottom: 16px;
	}

	p,
	ol,
	ul,
	figure {
		margin: 0 0 24px 0;
	}

	a,
	a:visited {
		color: #0087be;
	}

	a:hover,
	a:active,
	a:focus {
		color: #33bbe3;
	}


	/* Passion One */
        nav.title-bar {
		font-family: "Passion One", sans-serif;
		font-size: 26px;
	}

	.meta,
	.wp-caption-text {
		font-family: "Open Sans", sans-serif;
		font-size: 15px;
	}


	/* Meta */
	ul.meta {
		padding: 24px 0 0 0;
		margin: 0 0 24px 0;
	}

	ul.meta li {
		list-style: none;
		display: inline-block;
		margin: 0 8px 0 0;
		line-height: 24px;
	    white-space: nowrap;
	    overflow: hidden;
	    text-overflow: ellipsis;
	    max-width: 300px;
	}

	.meta,
	.meta a {
		color: #4f748e;
	}

	.byline amp-img {
		border-radius: 50%;
		border: 0;
		background: #f3f6f8;
		position: relative;
		top: 6px;
		margin-right: 6px;
    }


	/* Titlebar */
	nav.title-bar {
		background: #f8bbd0;
		padding: 0 16px;
	}

	nav.title-bar div {
		line-height: 46px;
		color: #fff;
	}

	nav.title-bar svg {
		fill: #fff;
		float: left;
		margin: 11px 8px 0 0;
	}


	/* Captions */
	.wp-caption-text {
		background: #eaf0f3;
		padding: 8px 16px;
	}


	/* Quotes */
	blockquote {
		padding: 16px;
		margin: 8px 0 24px 0;
		border-left: 2px solid #87a6bc;
		color: #4f748e;
		background: #e9eff3;
	}

	blockquote p:last-child {
		margin-bottom: 0;
	}
	</style>
</head>
<body>
<nav class="title-bar">
	<div>
           <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" version="1.1" width="24" height="24" viewBox="0 0 24 24"><path d="m14 22.5c-2.5-0.7-3.8-2.3-3.7-4.7 0-1 0.2-2 0.5-3.1 0.2-0.6 0.6-1.5 0.7-1.6 0.1 0 1 0.4 1.7 0.7 3.1 1.7 4.4 4 3.6 6.4-0.2 0.5-0.4 1-0.7 1.5l-0.2 0.4-0.2-0.1c-0.1-0.1-0.3-0.3-0.6-0.5l-0.4-0.4 0 0.1c0 0.1 0 0.4-0.1 0.8 0 0.4-0.1 0.6-0.1 0.6 0 0-0.2 0-0.4-0.1zm-10.2-2.7c-0.4-0.1-1.1-0.3-1.2-0.3 0 0 0.1-0.3 0.3-0.7 0.2-0.4 0.3-0.7 0.3-0.7 0 0-0.3 0.1-0.7 0.2-0.4 0.1-0.7 0.2-0.8 0.1 0 0 0-0.3 0-0.7 0-1.5 0.4-2.6 1.3-3.5 1-1 2.5-1.5 4.6-1.6 0.7 0 1.8 0 2.2 0.1l0.2 0 0 0.4c0 0.6-0.2 1.5-0.4 2.2-0.7 2.6-2 4.1-3.9 4.4-0.4 0.1-1.4 0.1-1.8 0zM18 15.2C16.7 15 15.3 14.3 14 13.3c-0.6-0.5-1.2-1-1.2-1 0-0.1 0.9-1.1 1.4-1.5 2.4-2.2 4.7-2.7 6.6-1.5 0.4 0.3 1.2 1 1.6 1.5l0.2 0.3-0.7 0.4-0.7 0.4 0.7 0.3c0.4 0.2 0.7 0.3 0.7 0.3 0.1 0.1-0.6 1-1.1 1.5-0.9 0.9-1.8 1.3-2.9 1.3-0.3 0-0.7 0-0.7 0zM7 11.5C4.6 11.2 3 10.3 2.2 8.7 2.2 8.5 2 8.2 2 7.9 1.8 7.5 1.8 7.3 1.8 6.5c0-0.6 0-1 0-1 0 0 0.3 0 0.8 0.1 0.4 0.1 0.7 0.2 0.7 0.2C3.4 5.8 3.3 5.5 3.1 5.2 3 4.8 2.8 4.5 2.8 4.5 2.8 4.4 2.9 4.4 3.3 4.3 5 3.9 6.4 4.1 7.5 5.1 8.8 6.2 9.7 8.3 9.9 11.1l0 0.3-0.2 0c-0.4 0.1-2.3 0.1-2.7 0zM11.5 10.8C11.3 10.5 11 9.8 10.8 9.1 9.8 6.4 10 4.3 11.4 2.9c0.4-0.4 0.8-0.7 1.4-1 0.4-0.2 1.5-0.6 1.5-0.5 0 0 0.1 0.3 0.1 0.7 0.1 1 0 1 0.7 0.3l0.6-0.5 0.2 0.3c0.5 0.7 0.9 1.5 1 2.2 0.1 0.3 0.1 0.5 0.1 1 0 0.6 0 0.7-0.2 1.1-0.5 1.4-1.7 2.7-3.4 3.8-0.4 0.2-1.5 0.8-1.5 0.8 0 0-0.1-0.1-0.1-0.2z" fill="#ffffff"/></svg>
            SakuraConnect</div>
</nav>
<div class="content">
	<h1 class="title"><?php echo esc_html( $amp_post->get_title() ); ?></h1>
	<ul class="meta">
		<li class="byline">
			<?php echo $amp_post->get_author_avatar(); ?>
			<span class="author"><?php echo $amp_post->get_author_name(); ?></span>
			<span>&nbsp;&nbsp;&bull;</span>
		</li>
		<li><time datetime="<?php echo esc_attr( $amp_post->get_machine_date() ); ?>"><?php echo esc_html( $amp_post->get_human_date() ); ?></time></li>
	</ul>
	<?php echo $amp_post->get_content(); ?>
</div>
<!-- <?php printf( 'Generated in %ss', timer_stop() ); ?> -->
</body>
</html>

<section id="skn-home-banner">
    <!-- Latest Headline -->
    <div class="headline">
        <div class="banner-list">
        <a ng-href="{{headline.latest_news.permalink}}"
           style="background-image: url('{{headline.latest_news.feature}}');">
           <div class="text-layer">
               <div class="badges">
                   <p ng-bind-html="headline.latest_news.category.name"></p>
                   <p>
                       <img
                           data-uk-tooltip="{pos:'bottom-right'}"
                           ng-show="{{headline.latest_news.tags.saFilipino}}"
                           title="Sa Filipino"
                           ng-src="{{headline.latest_news.tags.img}}" />
                   </p>
               </div>
               <div class="titles">
                    <h2 ng-bind-html="headline.latest_news.title"></h2>
               </div>
           </div>
        </a>
        </div>
    </div>
    <!-- Headline By Sections -->
    <div class="by-category">
        <?php for($x = 1; $x <= 4; $x++): ?>
        <div class="banner-list">
            <a ng-href="{{headline.category_<?php print $x; ?>.permalink}}"
               style="background-image: url('{{headline.category_<?php print $x; ?>.feature}}');">
                <div class="text-layer">
                    <div class="badges">
                        <p ng-bind-html="headline.category_<?php print $x; ?>.category.name"></p>
                        <p>
                            <img
                                data-uk-tooltip="{pos:'bottom-right'}"
                                ng-show="headline.category_<?php print $x ?>.tags.saFilipino"
                                title="Sa Filipino"
                                ng-src="{{headline.category_<?php print $x ?>.tags.img}}" />
                        </p>
                    </div>
                    <div class="titles">
                        <h2 ng-bind-html="headline.category_<?php print $x ?>.title"></h2>
                    </div>
                </div>
            </a>
        </div>
        <?php endfor; ?>
    </div>
</section>
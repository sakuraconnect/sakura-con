<div data-uk-sticky style="z-index:10000">
    <?php get_template_part('wp-template/nav','mobile'); ?>
    <?php get_template_part('wp-template/nav','pc'); ?>
</div>
<main ng-controller="singleMediumCtrl"
      id="skn-content">
    <article class="uk-article">
    <!-- Feature Image Box -->
        <header class="uk-width-1-1"
                 id="skn-feature-image"
                 style="background-image: url('<?php print get_that_image($post_id); ?>')">
            <div class="overlay"></div>
            <div class="header-content">
                <h1><?php the_title(); ?></h1>
                <p>
                    <span>
                        <a href="<?php print get_author_posts_url(get_the_author_meta('ID')) ?>"
                           target="_new">
                            <?php the_author_link(); ?></a>
                    </span> &bull;
                    <i class="uk-icon uk-icon-twitter"></i> 
                    <span>
                        <?php if(!empty(get_the_author_meta('twitter'))): ?>
                            @<a href="https://twitter.com/intent/follow?screen_name=<?php print get_the_author_meta('twitter'); ?>"
                                onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                target="_blank"><?php print get_the_author_meta('twitter'); ?></a>
                        <?php else: ?>
                            @<a href="https://twitter.com/intent/follow?screen_name=sakunnect"
                                onclick="javascript:window.open(this.href,  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
                                target="_blank">sakunnect</a>
                        <?php endif; ?>
                    </span>
                    <br />
                    Published <span class="datePublished"><?php the_date() ?> <?php the_time() ?></span> &bull; 
                    Updated <span><?php the_modified_date() ?> <?php the_modified_time() ?></span>
                </p>
            </div>
        </header>
        <section class="uk-width-1-1"
                 id="skn-content-container">
            <div class="uk-width-small-1-1
                        uk-width-medium-9-10">
            <?php get_template_part('wp-template/share','buttons'); ?>
            </div>
            <div class="skn-content
                        uk-width-small-1-1
                        uk-width-medium-9-10
                        content-styles">
                <?php the_content(); ?>
                <aside class="uk-panel 
                            uk-panel-box 
                            uk-panel-box-primary 
                            uk-width-1-1
                            uk-text-center 
                            uk-h4 
                            uk-text-strong">
                    <i class="uk-icon uk-icon-envelope-o"></i>
                    For any comments, suggestions and concerns on this post, send an email at comments@sakuraconnect.co
                </aside>                   
            </div>
        </section>
    </article>
</main>
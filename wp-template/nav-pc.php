<nav id="skn-pc-nav"
     class="uk-visible-large
            uk-width-1-1"
     ng-controller="pcHeaderCtrl">
    <div class="uk-width-9-10 nav-container">
        <div class="brand">
            <a href="<?php bloginfo('url'); ?>">
                <?php if(is_home()) : ?>
                    <h1><?php print bloginfo('name','display'); ?></h1>
                <?php else: ?>
                    <p><?php print bloginfo('name','display'); ?></p>
                <?php endif;?>
                <p><?php print bloginfo('description','display'); ?></p>
            </a>
        </div>
        <?php
        $mainNavMenu = array(
            'theme_location'  => 'skn-mainnav-menu',
            'menu'            => 'name',
            'container'       => 'div',
            'container_class' => 'menus',
            'container_id'    => '',
            'menu_class'      => 'uk-list',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
        );
            wp_nav_menu($mainNavMenu);
        ?>
        <div class="search uk-icon-search">
            <input type="search"
                   placeholder="Search Posts & Features"
                   ng-model="searchText"
                   ng-keydown="searchPosts($event)"
            />
        </div>
    </div>

</nav>
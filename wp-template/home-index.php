<?php
    get_template_part('wp-template/nav','mobile');
    get_template_part('wp-template/splash','modal');
?>
<main id="skn-content" ng-controller="homeCtrl">
    <?php get_template_part('wp-template/home','banner');?>
    <section id="skn-home-content"
             ng-class="{'curtain-down': drawCurtain}"
             class="curtain-start">
        <?php get_template_part('wp-template/nav','pc'); ?>
        <div id="skn-post-content"
             infinite-scroll="loadMorePosts()"
             infinite-scroll-disabled="disableInfiniteScroll">
            <div id="skn-sponsors"></div>
            <div class="post-container
                        uk-width-medium-9-10">
                <item-special data="specialPost"></item-special>
                <item-basic ng-repeat="post in allPosts" data="post"></item-basic>
            </div>
        </div>
    </section>
</main>

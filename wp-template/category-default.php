<?php $cat = get_query_var('cat'); ?>
<script>
    var catId       = <?php print $cat ?>;
    var catTitle    = "<?php single_cat_title('',true); ?>";
</script>
<div data-uk-sticky style="z-index:10000">
    <?php get_template_part('wp-template/nav','mobile'); ?>
    <?php get_template_part('wp-template/nav','pc'); ?>
</div>
<main ng-controller="categoryDefaultCtrl"
      id="skn-content">
    <!-- Category Title -->
    <section id="skn-category-meta">
        <div class="uk-width-medium-9-10
                    uk-width-small-1-1
                    uk-margin-top">
            <h1><?php single_cat_title('',true); ?></h1>
            <p>
                <i class="uk-icon-newspaper-o"></i>
                <?php
                    $category = get_queried_object();
                    echo $category->count;
                ?> Posts &bull;
                <i class="uk-icon-rss"></i>
                <a href="<?php get_category_rss_link(1,intval( get_query_var('cat'))); ?>">Subscribe</a>
            </p>
            <?php if(!empty(category_description())): ?>
                <p><?php print category_description(); ?></p>
            <?php endif; ?>
        </div>
    </section>
    <section id="skn-category-items"
             class="uk-width-1-1"
             infinite-scroll="loadMoreItems()"
             infinite-scorll-disabled="disableInfiniteScroll">
        <div class="uk-width-medium-9-10
                    uk-width-small-1-1
                    category-container">
            <item-basic ng-repeat="item in items" data="item"></item-basic>
        </div>
    </section>
</main>

<div id="skn-splash-modal"
     ng-controller="splashModalCtrl"
     ng-show="displayModal">
    <div id="skn-splash-container"
         class="uk-width-medium-6-10
                uk-width-small-1-1">
        <a href="#"
           class="uk-float-right 
                  uk-text-right 
                  uk-hidden-small"
           ng-click="closeScreen()">
            <i class="uk-icon uk-icon-close"></i>
            [CLOSE]
        </a>
        <div class="content">
            <?php
                print do_shortcode(get_option('skn_splash_modal_html','HTML Goes here',true));
            ?>
        </div>
        <div class="uk-text-center">
            <a href="#"
               ng-click="closeScreen()"
               class="uk-text-center
                      uk-button
                      uk-button-primary
                      uk-hidden-large
                      uk-width-1-1">
                CLOSE
            </a>
        </div>
    </div>
</div>
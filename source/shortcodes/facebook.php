<?php
add_shortcode('facebook','short_facebook');
function short_facebook($atts)
{
	extract($atts);
	ob_start();
?>
<div class="video-wrapper">
    <iframe src="//www.facebook.com/video/embed?video_id=<?php print get_post_meta(get_the_ID(),'id',true) ?>"
            frameborder="0"
            allowfullscreen></iframe>
</div>
<?php 
	$echo = ob_get_contents();
	ob_end_clean();
	return $echo;
}

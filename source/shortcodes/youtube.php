<?php
add_shortcode('youtube','short_youtube');
function short_youtube($atts)
{
	extract($atts);
	ob_start();
?>
<div class="video-wrapper">
    <iframe width="560" 
            height="315" 
            src="//www.youtube.com/embed/<?php print $id ?>?autoplay=0&controls=0" 
            frameborder="0" 
            allowfullscreen
            class="youtube-video">
    </iframe>
</div>
<?php 
	$echo = ob_get_contents();
	ob_end_clean();
	return $echo;
}

<?php

function skn_theme_options_init()
{
    //Headline Category Banner
    add_settings_section(
        'skn-banner-headline-options',
        'Headline Options',
        'skn_banner_section_cb',
        'sknThemeSettings'
    );

    //Footer Title Section
    add_settings_section(
        'skn-banner-footer-titles',
        'Footer Headline Titles',
        'skn_footer_section_cb',
        'sknThemeSettings'
    );

    //About Footer
    add_settings_section(
        'skn-footer-about-description',
        'Footer About Description',
        'skn_footer_about_section_cb',
        'sknThemeSettings'
    );

    //Modal HTML
    add_settings_section(
        'skn-splash-modal-html',
        'Splash Modal HTML',
        'skn_splash_modal_section_cb',
        'sknThemeSettings'
    );

    /*
     * Settings Declaration for the Modal Splash Section
     */
    register_setting(
        'skn-theme-options-group',
        'skn_splash_modal_html'
    );

    /*
     * Settings for the Field for Modal Splash Section
     */
    add_settings_field(
        'skn_modal_splash_html',
        'Splash Modal HTML',
        'skn_textbox_cb',
        'sknThemeSettings',
        'skn-splash-modal-html',
        array('option'=>'skn_splash_modal_html')
    );

    /*
     * Settings Declaration for Footer About Description
     */
    register_setting(
        'skn-theme-options-group',
        'skn_footer_about'
    );
    register_setting(
        'skn-theme-options-group',
        'skn_footer_about_link'
    );

    add_settings_field(
        'skn_footer_about',
        'Footer About Description',
        'skn_textbox_cb',
        'sknThemeSettings',
        'skn-footer-about-description',
        array('option'=>'skn_footer_about')
    );

    add_settings_field(
        'skn_footer_about_link',
        'Footer About Link',
        'skn_select_page_cb',
        'sknThemeSettings',
        'skn-footer-about-description',
        array('option'=>'skn_footer_about_link')
    );

    /*
     * Settings Declaration for Footer Title Options
     */
    for($x = 1;$x <= 3;$x++) {
        register_setting(
            'skn-theme-options-group',
            'skn_footer_title_'.$x
        );
    }

    /*
     * Setting the Field for Footer Title Options
     */
    for($x=1;$x <= 3; $x++){
        add_settings_field(
            'skn_headline_footer_'.$x.'_text',
            'Footer Title For '.$x,
            'skn_input_text_cb',
            'sknThemeSettings',
            'skn-banner-footer-titles',
            array('counter' => $x,'option'=>'skn_footer_title_')
        );
    }

    /*
     * Setting Declaration For Headline Options
     */
    for($x=1;$x <= 4;$x++) {
        register_setting(
            'skn-theme-options-group',
            "skn_headline_category_".$x
        );
    }

    /*
     * Setting the Field For Headline Options
     */
    for($x=1;$x <= 4;$x++){
        add_settings_field(
            'skn_headline_'.$x.'_text',
            "Category Banner $x",
            'skn_select_cat_cb',
            'sknThemeSettings',
            'skn-banner-headline-options',
            array('counter' => $x,'option'=>'skn_headline_category_')
        );
    }

}
add_action('admin_init','skn_theme_options_init');

function skn_input_text_cb($args)
{
    $option         = get_option($args['option'].$args['counter']);
    $option_name    = $args['option'].$args['counter'];
    $html           = '<input type="text" id="'.$option_name.'" value="'. $option .'" name="'.$option_name.'">';
    print $html;
}

function skn_select_cat_cb($args)
{
    $option         = get_option($args['option'].$args['counter']);
    $option_name    = $args['option'].$args['counter'];
    $params = array(
        'type'                     => 'post',
        'child_of'                 => 0,
        'parent'                   => '',
        'orderby'                  => 'name',
        'order'                    => 'ASC',
        'hide_empty'               => 1,
        'hierarchical'             => 1,
        'exclude'                  => '',
        'include'                  => '',
        'number'                   => '',
        'taxonomy'                 => 'category',
        'pad_counts'               => false
    );
    $categories     = get_categories($params);
    print '<select name="'.$option_name.'">';
    foreach($categories as $category){
        print '<option value="'.$category->term_id.'" '.selected($option,$category->term_id).'>';
        print $category->name;
        print '</option>';
    }
    print '</select>';
}

function skn_select_page_cb($args)
{
    $option         = get_option($args['option'].$args['counter']);
    $option_name    = $args['option'].$args['counter'];
    $params = array(
        'sort_order' => 'desc',
        'sort_column' => 'post_title',
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'meta_key' => '',
        'meta_value' => '',
        'authors' => '',
        'child_of' => 0,
        'parent' => -1,
        'exclude_tree' => '',
        'number' => '',
        'offset' => 0,
        'post_type' => 'page',
        'post_status' => 'publish'
    );
    $pages     = get_pages($params);
    print '<select name="'.$option_name.'">';
    foreach($pages as $page){
        print '<option value="'.$page->ID.'" '.selected($option,$page->ID).'>';
        print $page->post_title;
        print '</option>';
    }
    print '</select>';
}

function skn_textbox_cb($args)
{
    $option         = get_option($args['option']);
    $option_name    = $args['option'];
    print "<textarea rows='10' cols='50' name='$option_name' id='$option_name'>".$option."</textarea>";
}

function skn_banner_section_cb()
{
    print "<p>Setup category that will fetch the latest post in the category. Use Category slug.</p>";
}

function skn_footer_section_cb()
{
    print "<p>Headlines Call the Footers.</p>";
}

function skn_footer_about_section_cb()
{
    print "<p>Description of the footer</p>";
}

function skn_splash_modal_section_cb()
{
    print "<p>Set the HTML for the Modal. <i>Shortcodes work</i></p>";
}

function skn_theme_options(){
    add_theme_page(
        __('Theme Options','wpsettings'),
        __('Theme Options','wpsettings'),
        'edit_theme_options',
        'sknThemeSettings',
        'skn_theme_options_page'
    );
}
add_action('admin_menu','skn_theme_options');

function skn_theme_options_page()
{
    get_template_part('wp-template/settings/theme','options');
}
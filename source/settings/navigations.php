<?php
/*
 * Custom Navigation Controls in the WP Theme
 */


add_action( 'after_setup_theme', 'register_main_nav_menu' );
function register_main_nav_menu()
{
    register_nav_menu('skn-mainnav-menu',__('Main Page Menu','theme-slug'));
    for($x = 1; $x <= 3; $x++){
        register_nav_menu('skn-footernav-menu-'.$x,__('Footer Menu '.$x));
    }
}

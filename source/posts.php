<?php
function check_if_video_post($post_id)
{
    $tags = get_the_tags($post_id);
    $video_tag = 'video';
    if(!empty($tags)){
        foreach($tags as $tag)
        {
            if(strcasecmp($tag->name,$video_tag) == 0)
            {
                return true;
            }
        }
    }
    return false;
}
<?php
function call_author_items()
{
    $rsp = array();
    $current_page = (isset($_GET['page'])) ? esc_attr($_GET['page']) : 1;
    $params = array(
        'paged' => $current_page,
        'post_status' => 'publish',
        'post_per_page' => 10,
        'orderby' => 'modified',
        'order' => 'DESC',
        'author' => esc_attr($_GET['author'])
    );

    $q = new WP_Query($params);
    $x = 0;

    if($current_page <= $q->max_num_pages)
    {
        while ($q->have_posts()){
            $q->the_post();
            $rsp[$x]            = format_item_basic(get_the_ID());
            $x++;
        }
    }
    wp_reset_postdata();

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_author_items','call_author_items');
add_action('wp_ajax_call_author_items','call_author_items');
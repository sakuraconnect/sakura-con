<?php
function call_related_posts()
{
    $rsp = array();
    $postId = esc_attr($_GET['postId']);
    //Get all the Tags
    $tags     = get_the_tags(strval($postId));
    $tagIDs   = array();
    foreach($tags as $tag){
        $tagIDs[] = $tag->term_id;
    }

    $args = array(
        'post__not_in'  => array(strval($postId)),
        'tag__in'       => $tagIDs,
        'post_status'   => 'publish',
        'posts_per_page'=> 5
    );
    $q = new WP_Query($args);

    $x = 0;
    while($q->have_posts()){
        $q->the_post();
        $rsp[$x]['title']       = get_the_title();
        $rsp[$x]['permalink']   = get_the_permalink();
        $rsp[$x]['author']      = get_the_author();
        $rsp[$x]['date']        = get_the_date() .', '. get_the_time('g:i a');
        $x++;
    }

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_related_posts','call_related_posts');
add_action('wp_ajax_call_related_posts','call_related_posts');
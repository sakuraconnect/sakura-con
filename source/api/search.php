<?php
function call_search_items()
{
    $rsp = array();
    $params = array(
        'post_status'    => 'publish',
        'post_per_page'  => 13,
        'orderby'        => 'modified',
        'order'          => 'DESC',
        's'              => esc_attr($_GET['keyword'])
    );

    $q = new WP_Query($params);
    $x = 0;

    while ($q->have_posts()){
        $q->the_post();
        $rsp[$x]            = format_item_basic(get_the_ID());
        $x++;
    }
    wp_reset_postdata();

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_search_items','call_search_items');
add_action('wp_ajax_call_search_items','call_search_items');
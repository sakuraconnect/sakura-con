<?php
function call_tag_items()
{
    $rsp = array();
    $current_page = (isset($_GET['page'])) ? esc_attr($_GET['page']) : 1;
    $params = array(
        'paged'         => $current_page,
        'post_status'   => 'publish',
        'post_per_page' => 10,
        'orderby'       => 'modified',
        'order'         => 'DESC',
        'tag_id'        => esc_attr($_GET['tag'])
    );

    $q = new WP_Query($params);
    $x = 0;

    if($current_page <= $q->max_num_pages)
    {
        while ($q->have_posts()){
            $q->the_post();
            $rsp[$x] = format_item_basic(get_the_ID());
            $rsp[$x]['params']  = $params;
            $x++;
        }
    }
    wp_reset_postdata();

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_tag_items','call_tag_items');
add_action('wp_ajax_call_tag_items','call_tag_items');
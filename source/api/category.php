<?php
function call_category_items()
{
    $rsp = array();
    $current_page = (isset($_GET['page'])) ? esc_attr($_GET['page']) : 1;
    $params = array(
        'paged'         => $current_page,
        'post_status'   => 'publish',
        'post_per_page' => 10,
        'orderby'       => 'modified',
        'order'         => 'DESC',
        'cat'           => (int)esc_attr($_GET['cat'])
    );

    $q = new WP_Query($params);
    $x = 0;

    if($current_page <= $q->max_num_pages)
    {
        while ($q->have_posts()){
            $q->the_post();
            $rsp[$x]            = format_item_basic(get_the_ID());
            $x++;
        }
    }
    wp_reset_postdata();

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_category_items','call_category_items');
add_action('wp_ajax_call_category_items','call_category_items');

function call_category_items_count()
{
  $params = [
    'post_status' => 'publish',
    'cat'         => esc_attr($_GET['cat'])
  ];

  $q = new WP_Query($params);
  $rsp = [
    'post_count' => $q->$found_posts
  ];
  print json_encode($rsp);
  die();
}
add_action('wp_ajax_nopriv_call_category_items_count','call_category_items');
add_action('wp_ajax_call_category_items_count','call_category_items_count');

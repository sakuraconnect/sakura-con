<?php
function call_home_banner()
{
    $rsp        = array();
    $exclude_id = array();
    //Home Page
    $params     = array(
        'paged'          => 1,
        'post_status'    => 'publish',
        'post_per_page'  => 1,
        'orderby'        => 'modified',
        'order'          => 'DESC'
    );

    $q = new WP_Query($params);

    while($q->have_posts()){
        $q->the_post();
        $rsp['latest_news'] = format_item_basic(get_the_ID());
        $exclude_id[] = get_the_ID();
        break;
    }
    wp_reset_postdata();

    //category
    for($y=1; $y <= 4; $y++){
        $category = get_option("skn_headline_category_$y",'');
        if(!empty($category)){
            $params['post__not_in'] = $exclude_id;
            $params['cat']          = strval($category);
            $q = new WP_Query($params);
            while($q->have_posts()){
                $q->the_post();
                $rsp["category_$y"] = format_item_basic(get_the_ID());
                $exclude_id[] = get_the_ID();
                break;
            }
            wp_reset_postdata();
        }
    }
    $rsp['excluded_id'] = $exclude_id;

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_home_banner','call_home_banner');
add_action('wp_ajax_call_home_banner','call_home_banner');

function call_home_all_posts()
{
    $exception  = (isset($_GET['exceptions']))?esc_attr($_GET['exceptions']):'';
    $page       = (isset($_GET['page']))?esc_attr($_GET['page']):l;

    $rsp        = array();
    $params     = array(
        'paged'             => $page,
        'post_status'       => 'publish',
        'posts_per_page'    => ($page < 2)?10:11,
        'orderby'           => 'modified',
        'order'             => 'DESC',
        'post_per_page'     => 10,
        'post__not_in'      => array_map('intval',explode(',',$exception))
    );

    $q = new WP_Query($params);
    $x = 0;

    while($q->have_posts()){
        $q->the_post();
        $rsp[$x] = format_item_basic(get_the_ID());

        /*
         * Check Category for Special Icons
         */
        switch ($rsp[$x]['category']['slug']){
            case "opinions":
                $rsp[$x]['special']['img'] = get_template_directory_uri().'/assets/images/opinion.png';
                break;
            case "analysis":
                $rsp[$x]['special']['img'] = get_template_directory_uri().'/assets/images/editorial.png';
                break;
            case "diy":
                $rsp[$x]['special']['img'] = get_template_directory_uri().'/assets/images/diy.png';;
                break;
        }
        $x++;
    }
    wp_reset_postdata();

    print json_encode($rsp);
    die();
}
add_action('wp_ajax_nopriv_call_home_all_posts','call_home_all_posts');
add_action('wp_ajax_call_home_all_posts','call_home_all_posts');

<?php
function format_item_basic($post_id)
{
    $rsp = array();
    $rsp['title']       = get_the_title($post_id);
    $rsp['permalink']   = get_the_permalink($post_id);
    $rsp['author']      = get_the_author($post_id);
    $rsp['date']        = get_the_date() .' '. get_the_time('g:i a');

    $cat                    = end(get_the_category($post_id));
    $rsp['category']    = [
        'name'      => $cat->name,
        'permalink' => get_category_link($cat->term_id),
        'slug'      => $cat->slug

    ];
    $rsp['feature'] = get_that_image($post_id);

    $tags = get_the_tags($post_id);
    if($tags){
        foreach($tags as $tag){
            switch ($tag->name){
                case "Sa Filipino":
                    $rsp['tags']['saFilipino'] = true;
                    $rsp['tags']['img']        = get_template_directory_uri().'/assets/images/ph-badge.png';
                    break;
                case "video":
                    $rsp['tags']['video']      = true;
                    break;
            }
        }
    }

    return $rsp;
}
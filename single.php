<?php
  get_header();
  the_post();

  $post_id  = get_the_ID();
  $template = get_post_meta($post_id,'singles_template',true);
  $template = (empty($template))?'default':$template;
  get_template_part('wp-template/single',$template);

  get_footer();

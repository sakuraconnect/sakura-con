<?php
$DS = DIRECTORY_SEPARATOR;
require_once get_template_directory().$DS.'source'.$DS.'global.php';
require_once get_template_directory().$DS.'source'.$DS.'api'.$DS.'single.php';
require_once get_template_directory().$DS.'source'.$DS.'api'.$DS.'home.php';
require_once get_template_directory().$DS.'source'.$DS.'api'.$DS.'category.php';
require_once get_template_directory().$DS.'source'.$DS.'api'.$DS.'tag.php';
require_once get_template_directory().$DS.'source'.$DS.'api'.$DS.'author.php';
require_once get_template_directory().$DS.'source'.$DS.'api'.$DS.'search.php';
require_once get_template_directory().$DS.'source'.$DS.'settings'.$DS.'themeOptions.php';
require_once get_template_directory().$DS.'source'.$DS.'settings'.$DS.'navigations.php';
require_once get_template_directory().$DS.'source'.$DS.'shortcodes'.$DS.'youtube.php';
require_once get_template_directory().$DS.'source'.$DS.'shortcodes'.$DS.'facebook.php';
require_once get_template_directory().$DS.'source'.$DS.'posts.php';

/*
 * Remove fixed height on images
 * http://www.codeinwp.com/blog/wordpress-image-code-snippets/
 */
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
    $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
    return $html;
}

/*
 * get feature image
 */
function get_that_image($post_id, $is_thumb = false)
{
    $image = '';
    if(has_post_thumbnail())
    {
        $image = wp_get_attachment_url(get_post_thumbnail_id($post_id));
    }
    else
    {
        global $post, $posts;
        ob_start();
        ob_end_clean();

        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
        $image	= $matches[1][0];
        $pattern	= '#(-([1-9][0-9]{3}|[0-9]{3})[xX](([1-9][0-9]{3}|[0-9]{3})))#';

        $result		= preg_match($pattern,$image,$match);

        if($result !== 0)
        {
            $image = ($thumb)?preg_replace('#'.$match[0].'#', '-290x290', $image):$image;
        }
    }
    return $image;
}

/*
 * Remove P tags from Images
 */
function filter_ptags_on_images($content){
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

//Hook CSS and JS Here
function theme_default_styles()
{
    $template_uri = get_template_directory_uri().'/assets/js/';
    wp_enqueue_script('js-dependencies',$template_uri.'js-dependencies.js');

    //angular derivatives
    $template_uri = get_template_directory_uri().'/app/';
    wp_enqueue_script('angular-core',$template_uri.'config.js');
    wp_enqueue_script('angular-controller',$template_uri.'controllers.js');
    wp_enqueue_script('angular-directives',$template_uri.'directives.js');
    wp_enqueue_script('angular-services',$template_uri.'services.js');

    //common styles
    $template_uri = get_template_directory_uri().'/assets/css/';
    wp_enqueue_style('css-essentials',$template_uri.'css-essentials.css');
    wp_enqueue_style('google-fonts','//fonts.googleapis.com/css?family=Passion+One|Cabin+Condensed:400,500,600,700');
    wp_enqueue_style('google-fonts-main','//fonts.googleapis.com/css?family=Open+Sans:300,400,700');

    //de register built-in jquery
    wp_deregister_script('jquery');

    //CSS for Pages
    if(is_home()){
      wp_enqueue_style('sakunnect-home',$template_uri.'home.css');
    } else if (is_single() || is_page()) {
      $post_id    = get_the_ID();
      $template   = get_post_meta($post_id,'singles_template',true);
      $template   = (empty($template))?'default':$template;
      wp_enqueue_style('sakunnect-single',$template_uri."single-$template.css");
    } else if (is_category()){
        wp_enqueue_style('sakunnect-category',$template_uri.'category-default.css');
    } else if (is_tag()){
        wp_enqueue_style('sakunnect-tag',$template_uri.'tag-default.css');
    } else if (is_author()){
        wp_enqueue_style('sakkunect-author',$template_uri.'author-default.css');
    } else {
        wp_enqueue_style('sakkunect-404',$template_uri.'404.css');
    }
}
add_action('wp_enqueue_scripts','theme_default_styles');

function theme_default_title_settings($title,$separator)
{
    if(is_feed()){
        return $title;
    }

    global $page;

    //Add Tagline and Name of Blog
    $site_name          = get_bloginfo('name','display');
    $site_description   = get_bloginfo('description');
    $separator          = '&bull';

    $current_page_name = get_the_title(get_the_ID());

    if(is_single() || is_page()){
        $title = "$current_page_name $separator $site_name";
    }

    if($site_description && (is_home() || is_front_page())){
        $title = "$site_name $separator $site_description";
    }

    return $title;
}
add_filter('wp-title','theme_default_title_settings',10,2);

//Destroy Oringial Image and Only put Large
function replace_uploaded_image($image_data) {
    // if there is no large image : return
    if (!isset($image_data['sizes']['large'])) return $image_data;

    // paths to the uploaded image and the large image
    $upload_dir = wp_upload_dir();
    $uploaded_image_location = $upload_dir['basedir'] . '/' .$image_data['file'];
    $large_image_filename = $image_data['sizes']['large']['file'];

    // Do what wordpress does in image_downsize() ... just replace the filenames ;)
    $image_basename = wp_basename($uploaded_image_location);
    $large_image_location = str_replace($image_basename, $large_image_filename, $uploaded_image_location);

    // delete the uploaded image
    unlink($uploaded_image_location);

    // rename the large image
    rename($large_image_location, $uploaded_image_location);

    // update image metadata and return them
    $image_data['width'] = $image_data['sizes']['large']['width'];
    $image_data['height'] = $image_data['sizes']['large']['height'];
    unset($image_data['sizes']['large']);

    // Check if other size-configurations link to the large-file
    foreach($image_data['sizes'] as $size => $sizeData) {
        if ($sizeData['file'] === $large_image_filename)
            unset($image_data['sizes'][$size]);
    }

    return $image_data;
}
add_filter('wp_generate_attachment_metadata', 'replace_uploaded_image');

function skn_admin_color(){
    $theme_dir = get_stylesheet_directory_uri();

    wp_admin_css_color(
        'sakura-con',__('Sakura Connect'),
        $theme_dir.'assets/css/admin/sakura-colors.css',
        array('#384047','#5BC67B','#838cc7','#ffffff')
    );
}
add_action('admin_init','skn_admin_color');

//enable Ajax Functionality
function enable_ajax_functionality(){
    wp_localize_script('ajaxify',
        'ajaxify_function',
        array('ajaxurl' => admin_url('admin-ajax.php')));
}
add_action('template_redirect','enable_ajax_functionality');

//enable theme functionality
add_theme_support( 'post-thumbnails' );

//Enable title Flexibility
//https://teamtreehouse.com/library/seo-for-wordpress/enhancing-the-seo-of-a-wordpress-theme/title-tags-in-wordpress
add_theme_support( 'title-tag' );

/*
 *
 */
add_filter( 'img_caption_shortcode', 'cleaner_caption', 10, 3 );
function cleaner_caption( $output, $attr, $content )
{

    /* We're not worried abut captions in feeds, so just return the output here. */
    if (is_feed())
        return $output;

    /* Set up the default arguments. */
    $defaults = array(
        'id' => '',
        'align' => 'alignnone',
        'caption' => ''
    );

    /* Merge the defaults with user input. */
    $attr = shortcode_atts($defaults, $attr);

    /* If the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags. */

    //if (1 > $attr['width'] || empty($attr['caption']))
    //    return $content;

    /* Set up the attributes for the caption <div>. */
    $attributes = (!empty($attr['id']) ? ' id="' . esc_attr($attr['id']) . '"' : '');
    $attributes .= ' class="wp-caption uk-thumbnail ' . esc_attr($attr['align']) . '"';

    /* Open the caption <div>. */
    $output = '<div' . $attributes . '>';

    /* Allow shortcodes for the content the caption was created for. */
    $output .= do_shortcode($content);

    /* Append the caption text. */
    $output .= '<p class="uk-thumbnail-caption uk-margin-small">' . $attr['caption'] . '</p>';

    /* Close the caption </div>. */
    $output .= '</div>';

    /* Return the formatted, clean caption. */
    return $output;
}

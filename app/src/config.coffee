app = angular.module "sakura-con",['ngAnimate','ngSanitize','infinite-scroll']
app.config ($sceDelegateProvider) ->
  $sceDelegateProvider.resourceUrlWhitelist [
    'self'
    "https://www.youtube.com/embed/**"
    "https://www.facebook.com/video/**"
  ]
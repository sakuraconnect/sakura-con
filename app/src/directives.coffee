app.directive "tagList",(ajaxService) ->
  returnMe = {
    restrict    :'E'
    templateUrl :directiveUrl+'tagList.html'
    replace     :true
    scope       :{}
    link:(scope,elem,attr) ->
      scope.showList = false;
      params = {
        postId:attr.postId
        action:'call_related_posts'
      }
      q = ajaxService.getDataWithMoreData params
      q.success (data) ->
        scope.list = data
        scope.showList = if scope.list.length > 0 then true else false
  }

app.directive "itemBasic",(ajaxService) ->
  returnMe = {
    restrict    :'E'
    templateUrl :directiveUrl+'item-basic.html'
    replace     :true
    scope       :{
      data:'='
    }
    link:(scope,elem,attr) ->

  }

app.directive "searchPage",(ajaxService,$rootScope) ->
  returnMe = {
    restrict    : 'E'
    templateUrl : directiveUrl+'search.html'
    replace     : true
    scope       :{
      data:'='
    }
    link:(scope,elem,attr) ->
      scope.displaySearch = false
      scope.closePage = ->
        scope.displaySearch = false
      $rootScope.$on 'searchPage', (event,arg) ->
        scope.displaySearch     = true
        scope.loadSearchText    = true
        scope.searchText        = arg

        params = {
          keyword :arg
          action  :'call_search_items'
        }

        ajaxService.getDataWithMoreData(params).success (data) ->
          scope.items           = data
          scope.loadSearchText  = false
  }

app.directive "itemSpecial", ->
  returnMe = {
    restrict    : 'E'
    templateUrl : directiveUrl+'item-special.html'
    replace     : true
    scope       : {
      data:'='
    }
    link:(scope,elem,attr) ->

  }
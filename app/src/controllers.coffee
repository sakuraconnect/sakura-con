# Special Controller For Landing Page (Will be deleted once site goes live)
app.controller '404Ctrl',($scope) ->


app.controller 'mobileHeaderCtrl',($scope,$rootScope) ->
  $scope.toggleSearch = false
  $scope.toggleSearchFunc = ->
    $scope.toggleSearch = !$scope.toggleSearch

  $scope.searchPosts = (keyPress) ->
    if keyPress.keyCode is 13
      $rootScope.$emit 'searchPage',$scope.searchText

app.controller 'pcHeaderCtrl',($scope,$rootScope) ->
  $scope.searchPosts = (keyPress) ->
    if keyPress.keyCode is 13
      $rootScope.$emit 'searchPage',$scope.searchText

app.controller 'homeCtrl', ($scope,$timeout,helper,ajaxService) ->
  $scope.disableInfiniteScroll = true
  $scope.allPosts              = []
  params = {
    action    :'call_home_all_posts'
    page      : 1
  }
  ajaxService.getData('call_home_banner').success (data) ->
    $scope.headline     = data
    $scope.drawCurtain  = true

    params.exceptions = data.excluded_id.join()
    ajaxService.getDataWithMoreData(params).success (posts) ->
      $scope.specialPost  = posts[0]
      $scope.allPosts     = posts.slice 1
      $scope.disableInfiniteScroll = false

  $scope.loadMorePosts = ->
    $scope.disableInfiniteScroll = true
    params.page = params.page + 1
    ajaxService.getDataWithMoreData(params).success (posts) ->
      angular.forEach posts, (value,key) ->
        $scope.allPosts.push value
      $scope.disableInfiniteScroll = false

app.controller 'splashModalCtrl',($scope) ->
  isClicked = Cookies.get 'wasClicked'
  if(typeof isClicked is 'undefined' || isClicked is false)
    $scope.displayModal = true
  else
    $scope.displayModal = false

  $scope.closeScreen  = ->
    $scope.displayModal = !$scope.displayModal
    Cookies.set 'wasClicked',true,{expired:6000};

app.controller 'singleDefaultCtrl',($scope,helper) ->

app.controller 'singleMediumCtrl',($scope,helper) ->

app.controller 'categoryDefaultCtrl',($scope,helper,ajaxService) ->
  params = {
    action:'call_category_items'
    page:1
    cat:catId
  }

  params2 =
    action: 'call_category_items_count'
    cat   : catId
  ajaxService.getDataWithMoreData(params2).success (data) ->
    console.log data

  ajaxService.getDataWithMoreData(params).success (data) ->
    $scope.disableInfiniteScroll = false
    $scope.items = data
    params.max_page = data[0].max_pages

    $scope.loadMoreItems = ->
      $scope.disableInfiniteScroll = true
      params.page = params.page + 1
      ajaxService.getDataWithMoreData(params).success (data) ->
        angular.forEach data,(value,key) ->
          $scope.items.push value
        $scope.disableInfiniteScroll = false;

app.controller 'tagDefaultCtrl', ($scope,helper,ajaxService) ->
  params = {
    action:'call_tag_items'
    page:1
    tag:tagId
  }
  ajaxService.getDataWithMoreData(params).success (data) ->
    $scope.disableInfiniteScroll = false
    $scope.items = data

    $scope.loadMoreItems = ->
      $scope.disableInfiniteScroll = true;
      params.page = params.page + 1
      ajaxService.getDataWithMoreData(params).success (data) ->
        angular.forEach data,(value,key) ->
          $scope.items.push value
        $scope.disableInfiniteScroll = false;

app.controller 'authorDefaultCtrl', ($scope,helper,ajaxService) ->
  params = {
    action:'call_author_items',
    page:1,
    author:authorID
  }
  ajaxService.getDataWithMoreData(params).success (data) ->
    $scope.disableInfiniteScroll = false
    $scope.items = data

  $scope.loadMoreItems = ->
    $scope.disableInfiniteScroll = true
    params.page = params.page + 1
    ajaxService.getDataWithMoreData(params).success (data) ->
      angular.forEach data, (value,key) ->
        $scope.items.push value
      $scope.disableInfiniteScroll = false

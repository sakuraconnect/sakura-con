<!DOCTYPE html>
<html ng-app="sakura-con">
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport"
          content="width=device-width,
                   height=device-width,
                   initial-scale=1.0,
                   maximum-scale=1.0,
                   user-scalable=no">
    <link rel='shortcut icon'
          type='image/x-icon'
          href='<?php print get_template_directory_uri()."/assets/images/favicon.ico"; ?>' />
    <?php wp_head(); ?>
    <script>
        var ajaxurl         = "<?php print admin_url('admin-ajax.php'); ?>";
        var directiveUrl    = "<?php print get_template_directory_uri().'/template/directives/'?>";
    </script>
</head>
<body>

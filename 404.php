<?php
  get_header();
  get_template_part('wp-template/nav','pc');
  get_template_part('wp-template/nav','mobile');
?>
<main ng-controller="404Ctrl"
      class="uk-flex
             uk-flex-middle
             uk-flex-center
             uk-text-center">
  <section>
    <img   src="https://placeholdit.imgix.net/~text?txtsize=80&bg=dadada&txt=%E3%81%84%E3%81%84%E3%81%88%EF%BC%81&w=500&h=100&txttrack=10"
         alt="404 image placeholder"
         class="uk-align-center"/>
    <h1 class="uk-text-center">404: Page not found</h1>
    <p>You don't believe that the page you're looking for is nowhere to be seen? The feeling is mutual.</p>
    <p>Use the navbar to search other posts, navigate to other categories or go back to the homepage.</p>
    <p>Having happy thoughts also give you more chances of getting to the page you want to see.</p>
  </section>
</main>
<?php get_footer(); ?>
